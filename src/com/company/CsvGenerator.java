package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class CsvGenerator implements ToCsvGenerator {
    final String fileName = "new.csv";

    @Override
    public void ToCsv(List<String> lines) throws IOException {
        FileWriter CsvWriter = new FileWriter(fileName);
        // pretty line
        CsvWriter.append("*************");
        CsvWriter.append("\n");
        // new line + '\n'
        for (int i = 0; i < lines.size(); i++)
            CsvWriter.append(lines.get(i));
        CsvWriter.append("\n");
        // pretty line
        CsvWriter.append("*************");
        CsvWriter.append("\n");
        CsvWriter.flush();
        CsvWriter.close();
    }
}
