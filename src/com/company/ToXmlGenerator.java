package com.company;

import java.util.List;

public interface ToXmlGenerator {
    void ToXml(List<String> lines);
}
