package com.company;

import java.io.IOException;
import java.util.Arrays;

class Main {
    public static void main(String[] args) {
        CsvGenerator csvGenerator = new CsvGenerator();
        try {
            csvGenerator.ToCsv(Arrays.asList("first line", "second line", "third line"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
