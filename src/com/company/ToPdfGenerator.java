package com.company;

import java.util.List;

public interface ToPdfGenerator {
    void ToPdf(List<String> lines);
}
